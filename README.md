# DOI change management checker
Experimental to sanity check DOI change management files.

## Samples

### Transfer file
This file is used to change the background "owner" prefix against the DOI and the type of file can be identified by the `fromPrefix=10.32013;toPrefix=10.5555` part of the header. It would just have a list of DOIs below the header which would be updated based on the header above.

##### Possible Errors:
* The `fromPrefix` is different to the one that is currently assigned against the DOI.

* The DOI is not registered in Crossref.

### URL update files
This file would be used to update the URLs against DOIs. It would use the same header as the _transfer file_ but would also contain the URLs next to the DOIs separated by a tab.

##### Possible Errors:
* The DOIs and URLs are not separated by a tab, instead a space has been used. The system would think that the whole string is the DOI causing it to think it is not registered in the system.

* The `fromPrefix` is different to the one that is currently assigned against the DOI.

* The DOI is not registered in Crossref.

### Alias DOIs 
This file is used to make the alias relationship between two DOIs. There can be multiple primary DOIs and the alias DOIs listed below the header separated by a tab.
You can identify the file by the header which includes the `op=force_alias` section.

##### Possible Errors:
* DOIs are not separated by a tab

* DOIs are already in an alias relationship

### Unalias DOIs
This file is used to un-alias a current alias relationship between two DOIs. You would only need to include the current **alias** DOI in the list
You can identify the file by the header which includes the `op=unalias` section.

##### Possible Errors:
* DOIs are not in an alias relationship

* DOI is the primary in the alias relationship

### Timestamp Reset 
This file is used to reset the timestamp against DOIs. Each DOI has a timestamp against it which can be determined on a whole journal basis or individually for each DOI. Sometimes when moving to a different deposit method the timestamp needs resetting.

The file can be indentified by the header part `type=reset_timestamps`.

##### Possible Errors:
* DOI is not registered on Crossref

