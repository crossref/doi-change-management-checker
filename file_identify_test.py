#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 11:40:06 2022

@author: pauldavis
"""


# with open(fn) as f:
#   content = f.readlines()
#   sanity_check(content)
  
# def parse_header(headerline):
#   header = {entry.split("=")[0]: entry.split("=")[1] for entry in [entry for entry in headerline.split(";")]}
#   file_identify(header)
#   return header

# def sanity_check(content):
#     global header
#     header = parse_header(content.pop(0))
#     print (header)



filetypes = {
    'transfer/url' : ['fromPrefix', 'toPrefix'],
    'book_merge': ['toBook'],
    'resolve_as_is' : ['resolve'],
    'alias' : ['force_alias','delim=tab'],
    'set_as_primary': ['primary'],
    'journal_merge' : ['toJournal'],
    'series_merge' : ['toSeries'],
    'timestamp_reset' : ['reset_timestamp', 'timestamp='],
    'unalias' : ['unalias', 'delim=tab'] 
    
}

def file_type(header):
    counter = 0
    for file_type, clues in filetypes.items():
        for clue in clues:        
            if clue in header:
                counter += 1
                if counter == len(filetypes[file_type]):
                    if file_type == 'transfer/url' and "transfer_owner" in header:
                        return "transfer"
                    elif file_type == 'transfer/url' and "transfer_owner" not in header:
                        return "url_update"
                    else:
                        return file_type
                elif counter == 0:
                    return None
                
                
as_is = 'H:email=support@crossref.org;op=resolve'
alias = 'H:email=pdavis@crossref.org;op=force_alias;delim=tab'
primary = 'H:email=support@crossref.org;op=primary'
book_merge = 'H:email=support@crossref.org;type=transfer_title;toBook=266443'
doi_transfer = 'H: email=pdavis@crossref.org;type=transfer_owner;fromPrefix=10.30965;toPrefix=10.1163'
journal_merge = 'H: email=support@crossref.org;type=transfer_title;toJournal=313129'
series_merge = 'H: email=support@crossref.org;type=transfer_title;toSeries=313129'
timestamp = 'H:email=pdavis@crossref.org;type=reset_timestamps;timestamp=2021'
unalias = 'H:email=pdavis@crossref.org;op=unalias;delim=tab'
url_update = 'H:email=pdavis@crossref.org;fromPrefix=10.5762;toPrefix=10.5762'


assert file_type(as_is) == 'resolve_as_is'
assert file_type(alias) == 'alias'
assert file_type(primary) == 'set_as_primary'
assert file_type(book_merge) == 'book_merge'
assert file_type(doi_transfer) == 'transfer'
assert file_type(journal_merge) == 'journal_merge'
assert file_type(series_merge) == 'series_merge'
assert file_type(timestamp) == 'timestamp_reset'
assert file_type(unalias) == 'unalias'
assert file_type(url_update) == 'url_update'
assert file_type("foo") == None

# with open(fn) as f:
#   content = f.readlines()
#   sanity_check(content)













