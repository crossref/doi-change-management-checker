#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 22:09:07 2021

@author: pauldavis
"""

import requests
import urllib
import os
from collections import Counter
from tqdm import tqdm

def parse_header(headerline):
    header = {entry.split("=")[0]: entry.split("=")[1] for entry in [entry for entry in headerline.split(";")]}
    file_identify(header)
    return header

def extract_member_id_from_uri(uri):
  parsed = urllib.parse.urlsplit(uri)
  return os.path.basename(parsed.path)

def get_prefix_owner_member_id(prefix):
  """
  Given DOI prefix, return member_id
  """
  prefix_record = requests.get(f"https://api.crossref.org/prefixes/{prefix}").json()
  member_uri = prefix_record['message']['member']
  member_id = extract_member_id_from_uri(member_uri)
  return member_id


def get_prefix_owner_info(prefix):
  """
  given a prefix lookup the member_info and return it
  """
  member_id = get_prefix_owner_member_id(prefix)
  return requests.get(f"https://api.crossref.org/members/{member_id}").json()['message']


def extract_column_one(content):
    """
    extracts the first column from the file
    """
    return [line.split("\t")[0] for line in content]

def extract_column_two(content):
    """
    extracts the second column from the file (could be URLs or more DOIs)
    """
    return [line.split("\t")[1] for line in content]

def domain_counts(uris):
  return Counter([urllib.parse.urlsplit(uri).hostname for uri in uris])

def sanity_check_op_two_dois(content):
    primary_dois = extract_column_one(content)
    alias_dois = extract_column_two(content)
    full_list_dois = primary_dois + alias_dois
    output = []
    for doi in tqdm(full_list_dois):
        api_call = "http://api.crossref.org/works/" + doi
        results = requests.get(api_call)
        if results.status_code == 404:
            output.append(doi)
            print (f"Out of {len(full_list_dois)} Primary DOIs, {len(output)} DOI/DOIs are not registered with us:\n{chr(10).join(output)}")
     
    
def sanity_check_op_one_dois(content):
    doi_list = extract_column_one(content)
    output = []
    for doi in tqdm(doi_list):
        api_call = "http://api.crossref.org/works/" + doi
        results = requests.get(api_call)
        if results.status_code == 404:
            output.append(doi)
            print (f"Out of {len(doi_list)} Primary DOIs, {len(output)} DOI/DOIs are not registered with us:\n{chr(10).join(output)}")


def header_type(header):
    pass

def header_op(header):
    if 'force_alias' or 'resolve' or 'primary' in header.values():
        print ('This file is two DOI list, either alias file, resolve conflict as is or assign primary')
        sanity_check_op_two_dois(content)
    else:
        print ('Single column file')
        sanity_check_op_one_dois(content)


def sanity_check(content):
    global header
    header = parse_header(content.pop(0))
    print (header)
        
def header_transfer():
    from_info = get_prefix_owner_info(header['fromPrefix'])
    from_name = from_info['primary-name']

    to_info = get_prefix_owner_info(header['toPrefix'])
    to_name = to_info['primary-name'] 
    if from_name != to_name:
        print(f'Transfer of {len(content)} DOIs from "{from_name}" to "{to_name}"')
    else:
        print(f'Updating resources for {from_name}')
    dois = extract_column_one(content)
    urls = extract_column_two(content)
    print ("-" * 80)

  # Show what domains things will be redirected to
    print("DOIs will be redirected to the following domains:")
    for domain, count in domain_counts(urls).items():
        print(f"{domain} ({count} times)")
      # Make sure all the DOIs belong to the fromPrefix
    do_not_belong = []
    prefixes = []
    for doi in dois:
        prefix, *suffix = doi.split("/")
        prefixes.append(prefix)
    
    if prefix not in to_info['prefixes']:
      do_not_belong.append(doi)
    print(f"WARNING: {len(do_not_belong)} DOIs do not appear to belong to {to_name}")
    print ("-" * 80)
    print(Counter(prefixes))

def file_identify(header):
    if "op" in header:
        header_op(header)
    elif "type" in header:
        header_type(header)
    else:
        header_transfer(header)





    
    
    
    

with open(fn) as f:
  content = f.readlines()
  sanity_check(content)
 